
// window.pony

class Window

  var sdl_window: Pointer[SDLWindow] ref
  var _env: Env
  var _event: Pointer[SDLEvent] ref

  new create(env: Env) =>
    _env = env
    sdl_window = Pointer[SDLWindow].create()
    _event = @make_sdl_event()

  fun ref init( name: String, width: U32, height: U32) =>
    @SDL_GL_SetAttribute(SDLMajorVersion(), 3)
    @SDL_GL_SetAttribute(SDLMinorVersion(), 1)
    @SDL_GL_SetAttribute(SDLProfileMask(), SDLProfileCore())
    if @SDL_Init(SDLInitVideo()) < 0 then
      _env.out.print("SDL could not be initialized")
      print_sdl_error()
      return
    end

    @SDL_GL_SetAttribute(SDLGlDepthSize(), 16)

    sdl_window = @SDL_CreateWindow(
      name.cstring(),
      SDLWindowPosCentered(),
      SDLWindowPosCentered(),
      width,
      height,
      (SDLWindowOpenGL()
        or SDLWindowShown()
        or SDLWindowResizable()
        // uncomment this line for fullscreen:
        /* or SDLWindowFullscreenDesktop() */
      ))
    if sdl_window.is_null() then
      _env.out.print("Window could not be created")
      print_sdl_error()
      return
    end

    let context = @SDL_GL_CreateContext(sdl_window)
    if context.is_null() then
      _env.out.print("GL context could not be created")
      print_sdl_error()
      return
    end

    @glewInit()

    @glEnable(GLDebugOutput())
    @glEnable(GLDepthTest())
    @glDepthFunc(GLLess())
    @gl_debugging[None]()

  // is this function necessary?
  // I'd like to replace it with process_errors
  fun print_sdl_error() =>
    // we *do* have access to _env, but I'm
    // not sure you can safely read returned cstrings.
    @printf[None]("%s\n".cstring(), @SDL_GetError())

  fun has_quit_event(): Bool =>
    while @SDL_PollEvent(_event) != 0 do
      if @is_sdl_quit_event(_event) == 1 then return true end
    end
    false

  fun process_errors() =>
    @process_errors[None]()

  fun swap() =>
      @SDL_GL_SwapWindow(sdl_window)
      process_errors()

  fun ref finish() =>
    @free_sdl_event(_event)
    @SDL_DestroyWindow(sdl_window)
    sdl_window = Pointer[SDLWindow].create()
    @SDL_Quit()
