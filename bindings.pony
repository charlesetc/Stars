
// bingings.pony

use "lib:SDL2"
use "lib:GLEW"
use "lib:GL"
use "path:/home/charles/code/stars"
use "lib:stars_extension"

use @SDL_GL_SetAttribute[None](attr: U32, value: U32)
use @SDL_Init[I32](mode: U32)
use @SDL_GetError[Pointer[U8]]()
use @SDL_CreateWindow[Pointer[SDLWindow]](
  name: Pointer[U8] tag,
      x: U32,
      y: U32,
      width: U32,
      height: U32,
      mode: U32)
use @SDL_DestroyWindow[None](window: Pointer[SDLWindow] box)
use @SDL_Quit[None]()
use @SDL_PollEvent[U32](i: Pointer[SDLEvent] box)
use @SDL_GetTicks[U32]()
use @SDL_GL_SwapWindow[None](window: Pointer[SDLWindow] box)
use @SDL_GL_CreateContext[Pointer[SDLWindow]](window: Pointer[SDLWindow])

use @glewInit[None]()
use @glClearColor[None](r: F32, g: F32, b: F32, a: F32)
use @glClearDepthf[None](depth: F32)
use @glClear[None](mode: U32)

use @make_sdl_event[Pointer[SDLEvent]]()
use @is_sdl_quit_event[I32](event: Pointer[SDLEvent] box)
use @free_sdl_event[None](event: Pointer[SDLEvent])

use @glCreateProgram[U32]()
use @glCreateShader[U32](kind: U32)
use @glCompileShader[None](shader: U32)
use @glShaderSource[U32](shader: U32, count: USize, string: Pointer[Pointer[U8] tag], length: Pointer[None])
use @glAttachShader[None](program: U32, shader: U32)
use @glLinkProgram[None](program: U32)
use @glUseProgram[None](program: U32)
use @glBindVertexArray[None](id: U32)
use @glGenBuffers[None](n: I32, pointer: Pointer[U32])
use @glBindBuffer[None](kind: U32, buffer: U32)
use @glBufferData[None](kind: U32, size: USize, array: Pointer[None] tag, mode: U32)
use @glDrawArrays[None](kind: U32, buffer: U32, number: U32)
use @glDrawElements[None](kind: U32, number: U32, datatype: U32, indices: U32)
use @glGetUniformLocation[U32](program_id: U32, attr: Pointer[U8] tag)
use @glGetAttribLocation[U32](program_id: U32, attr: Pointer[U8] tag)
use @glVertexAttribPointer[None](
  index: U32, size: U32, type': U32,
  normalized: U8, stride: I32, pointer: U32
)
use @glEnableVertexAttribArray[None](attribute_id: U32)
use @glEnable[None](mode: U32)
use @glDepthMask[None](should_do: U8)
use @glDepthFunc[None](mode: U32)
use @glUniform1f[None](id: U32, f: F32)

// Matrices
use @glUniformMatrix4fv[None](id: U32, count: U32, transpose: U8, value: Pointer[F32] tag)
use @glm_value_ptr[Pointer[F32]](p: Pointer[Mat4] box)
use @glm_mat4_print[None](p: Pointer[Mat4] box)
use @glm_mat4_mul[Pointer[Mat4]](a: Pointer[Mat4] box, b: Pointer[Mat4] box)
use @glm_vec3[Pointer[Vec3]](x: F32, y: F32, z: F32)
use @glm_mat4[Pointer[Mat4]](fill: F32)
use @glm_mat4_rotate[Pointer[Mat4]](m: Pointer[Mat4] box, angle: F32, axis: Pointer[Vec3])
use @glm_mat4_translate[Pointer[Mat4]](m: Pointer[Mat4] box, axis: Pointer[Vec3])
use @glm_mat4_inverse[Pointer[Mat4]](m: Pointer[Mat4] box)
use @glm_mat4_transpose[Pointer[Mat4]](m: Pointer[Mat4] box)
use @glm_mat4_scale[Pointer[Mat4]](m: Pointer[Mat4] box, f: F32)
use @glm_look_at[Pointer[Mat4]](x: Pointer[Vec3], y: Pointer[Vec3], z: Pointer[Vec3])
use @glm_perspective[Pointer[Mat4]](a: F32, b: F32, c: F32, d: F32)

primitive NULL fun apply(): Pointer[None] => Pointer[None].create()

primitive GLColorBufferBit fun apply(): U32 => 0x4000
primitive GLDepthBufferBit fun apply(): U32 => 0x100
primitive GLArrayBuffer    fun apply(): U32 => 0x8892
primitive GLElementBuffer  fun apply(): U32 => 0x8893
primitive GLStaticDraw     fun apply(): U32 => 0x88E4
primitive GLTriangles      fun apply(): U32 => 0x0004
primitive GLFragmentShader fun apply(): U32 => 0x8B30
primitive GLVertexShader   fun apply(): U32 => 0x8B31
primitive GLCompileStatus  fun apply(): U32 => 0x8B81
primitive SDLGlDepthSize   fun apply(): U32 => 6
primitive GLLinkStatus     fun apply(): U32 => 0x8B82
primitive GLUnsignedInt    fun apply(): U32 => 0x1405
primitive GLDepthTest      fun apply(): U32 => 0x0B71
primitive GLLess           fun apply(): U32 => 0x0203
primitive GLFloat          fun apply(): U32 => 0x1406
primitive GLFalse          fun apply(): U8  => 0
primitive GLTrue           fun apply(): U8  => 1
primitive GLDebugOutput    fun apply(): U32 => 0x92E0

primitive SDLProfileCore  fun apply(): U32 => 1
primitive SDLMajorVersion fun apply(): U32 => 17
primitive SDLMinorVersion fun apply(): U32 => 18
primitive SDLProfileMask  fun apply(): U32 => 21


primitive SDLWindowOpenGL    fun apply(): U32 => 2
primitive SDLWindowShown     fun apply(): U32 => 4
primitive SDLWindowResizable fun apply(): U32 => 0x20
primitive SDLWindowFullscreenDesktop fun apply(): U32 => 0x1001

primitive SDLWindowPosCentered fun apply(): U32 => 0x2FFF0000

primitive SDLInitVideo    fun apply(): U32 => 20

primitive SDLWindow
primitive SDLEvent
primitive Vec3
primitive Mat4
