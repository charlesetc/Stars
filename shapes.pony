
// shapes.pony

class ShapeSystem

  var _shapes: Array[Shape]
  var _shader_program: ShaderProgram

  new create(shapes: Array[Shape], shader_program: ShaderProgram) =>
    _shapes = shapes
    _shader_program = shader_program
    _shader_program()

    for shape in _shapes.values() do

      @glGenBuffers(1, addressof shape.vbo)
      @glBindBuffer(GLArrayBuffer(), shape.vbo)
      @glBufferData(
        GLArrayBuffer(),
            // SIZEOF != SIZE
            // * 32 is the size of the float...
            // make this cleaner please
            shape.vertices.size() * 32,
                shape.vertices.cpointer(),
                GLStaticDraw()
      )

      @glGenBuffers(1, addressof shape.ebo)
      @glBindBuffer(GLElementBuffer(), shape.ebo)
      @glBufferData(
        GLElementBuffer(),
            shape.elements.size() * 32,
            shape.elements.cpointer(),
            GLStaticDraw()
      )

    end

  fun draw() =>
    for shape in _shapes.values() do
      @glBindBuffer(GLArrayBuffer(), shape.vbo)
      @glBindBuffer(GLElementBuffer(), shape.ebo)
      _shader_program() // use and define attributes
      @glDrawElements(
        GLTriangles(),
        shape.elements.size().u32(),
        GLUnsignedInt(),
        0
      )
    end

class Shape
  """
  Basically just a container
  for vertices and elements.
  The idea is that a `Shape`
  is a collection of triangles
  that may or may not share
  vertices.
  """

  var vbo: U32 = 0
  var ebo: U32 = 0
  var vertices: Array[F32]
  var elements: Array[U32]

  new create(vertices': Array[F32], elements': Array[U32]) =>
    vertices = vertices'
    elements = elements'
