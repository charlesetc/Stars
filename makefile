
run: ./stars
	./stars

./stars: ./stars_extension.o *.pony makefile
	ponyc -d

./stars_extension.o: stars_extension.cc
	clang -lstdc++ -fPIC -Wall -Wextra -O3 -g -MM stars_extension.cc >stars_extension.d
	clang -lstdc++ -fPIC -Wall -Wextra -O3 -g   -c -o stars_extension.o stars_extension.cc
	clang -lstdc++ -shared -o libstars_extension.so stars_extension.o

clean:
	rm -f stars_extension.d
	rm -f stars_extension.o
	rm -f libstars_extension.so
	rm stars
