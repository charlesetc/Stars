#version 130
out vec4 outColor;
in vec3 v_normal;
in vec3 v_position;
in float Time;

void main()
{
  vec3 v_light = vec3(-100, 0, 100);
  vec3 norm = normalize(v_normal);
  vec3 light = normalize(v_light - v_position);
  float brightness = dot(norm, light);
  // brightness = max(ceil(brightness * 3) / 1, 0.0);

  vec3 camera_dir = normalize(-v_position);
  vec3 half_dir = normalize(normalize(v_light) + camera_dir);
  float specular = pow(max(dot(half_dir, normalize(v_normal)), 0.0), 16.0);

  vec3 dark_color  = vec3(0, 0.3, 0.3);
  vec3 light_color = vec3(0, 0.8, 0.8) + int((brightness > 0.98)) * vec3(1,1,1);

  vec3 color = mix(dark_color, light_color, brightness);

  outColor = vec4(color, 1);
}
