
#include <glm/vec3.hpp> // glm::vec3
/* #include <glm/vec4.hpp> // glm::vec4 */
#include <glm/mat4x4.hpp> // glm::ma
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

extern "C" {

#include "./stars_extension.h"
#include <GL/glew.h>
#include <GL/gl.h>

SDL_Event *make_sdl_event()
{
  return (SDL_Event *)malloc(sizeof(SDL_Event));
}

int is_sdl_quit_event(SDL_Event *e)
{
  return (*e).type == SDL_QUIT;
}

void free_sdl_event(SDL_Event *e)
{
  free(e);
}

void error_shader_log(int shader) {
  char buffer[512];
  glGetShaderInfoLog(shader, 512, NULL, buffer);

  printf("shader compilation error\n%s\n", buffer);
}

void error_program_log(int program) {
  char buffer[512];
  glGetProgramInfoLog(program, 512, NULL, buffer);

  printf("program compilation error\n%s\n", buffer);
}


int times = 0;
void process_errors(void) {
  GLenum err;
  while((err = glGetError()) != GL_NO_ERROR)
  {
    printf("GL Error number: %d, on trial %d\n", err, times);
  }
  times += 1;
}
void debug_callback(GLenum source, GLenum type, GLuint id,
       GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {

  if (severity >= 37190) {
    printf("source: %d\n", source);
    printf("type: %d\n", type);
    printf("severity: %d\n", severity);
    printf("message: %s\n\n", message);
  }
}

void gl_debugging(void) {
  glDebugMessageCallback(debug_callback, NULL);
}

int float_size(void) {
  return sizeof(float);
}

// glm content
// basically this is a c-compatible
// (and therefor pony-compatible)
// wrapper for the parts of the c++ glm
// library that are needed.

typedef glm::mat4 * matrix;
typedef glm::vec3 * vector;

vector glm_vec3(float x, float y, float z) {
  vector v = (vector)malloc(sizeof(glm::vec2));
  *v = glm::vec3(x, y , z);
  return v;
}

void glm_free(void *v) {
  free(v);
}

void glm_mat4_print(matrix m) {
  std::cout << glm::to_string(*m) << std::endl;
}

// have to figure out times, for sure
matrix glm_mat4(float fill) {
  matrix m = (matrix)malloc(sizeof(glm::mat4));
  *m = glm::mat4(fill);
  return m;
}

matrix glm_mat4_mul(matrix a, matrix b) {
  matrix m = (matrix)malloc(sizeof(glm::mat4));
  *m = *a * *b;
  return m;
}

matrix glm_mat4_rotate(matrix mat, float angle, vector axis) {
  matrix m = (matrix)malloc(sizeof(glm::mat4));
  *m = glm::rotate(*mat, glm::radians(angle), *axis);
  return m;
}

matrix glm_mat4_translate(matrix mat, vector axis) {
  matrix m = (matrix)malloc(sizeof(glm::mat4));
  *m = glm::translate(*mat, *axis);
  return m;
}

matrix glm_mat4_inverse(matrix mat) {
  matrix m = (matrix)malloc(sizeof(glm::mat4));
  *m = glm::inverse(*mat);
  return m;
}

matrix glm_mat4_transpose(matrix mat) {
  matrix m = (matrix)malloc(sizeof(glm::mat4));
  *m = glm::transpose(*mat);
  return m;
}

matrix glm_mat4_scale(matrix mat, float f) {
  matrix m = (matrix)malloc(sizeof(glm::mat4));
  *m = glm::scale(*mat, glm::vec3(f));
  return m;
}

matrix glm_look_at(vector x, vector y, vector z) {
  matrix m = (matrix)malloc(sizeof(glm::mat4));
  *m = glm::lookAt(*x, *y, *z);
  return m;
}

matrix glm_perspective(float angle, float aspect, float znear, float zfar) {
  matrix m = (matrix)malloc(sizeof(glm::mat4));
  *m = glm::perspective(glm::radians(angle), aspect, znear, zfar);
  return m;
}

void *glm_value_ptr(matrix m) {
  return glm::value_ptr(*m);
}

} // extern "C"
