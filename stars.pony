
// stars.pony


actor Game

  var _env: Env
  var _shape_system: ShapeSystem
  var _window: Window

  var _time: Uniform
  var _n_matrix: Uniform
  var _m_matrix: Uniform
  var _v_matrix: Uniform
  var _p_matrix: Uniform

  new create(env: Env) =>
    _env = env
    _window = Window.create(_env)
    _window.init("Stars", 1200, 1000)

    let o = Obj(env, "object_files/pig.obj")

    let suzanne = Shape(
      o.vertices_and_normals(),
      o.elements()
    )

    let shader_program = ShaderProgram(
      _env,
      "shader.vert",
      "shader.frag",
      [
        DefineAttribute("position", 3)
        DefineAttribute("normal", 3)
        /* DefineAttribute("color", 3) */
      ]
    )

    _shape_system = ShapeSystem([suzanne], shader_program)

    _time = shader_program.uniform("time")
    _n_matrix = shader_program.uniform("N")
    _m_matrix = shader_program.uniform("M")
    _v_matrix = shader_program.uniform("V")
    _p_matrix = shader_program.uniform("P")

  be apply() =>
    Graphics.clear(0.4, 0.4, 0.4, 1.0)
    let t = Graphics.time()
    _time.set_float(t)

    let view = Matrix.look_at(
      Vector(-1.2,1.2,1.5),
      Vector(0.0, 0.0, 0.0),
      Vector(0.0, 0.0, 1.0)
    )
    let projection = Matrix.perspective(
      // the angle
      60,
      // the aspect ratio
      1.2,
      1,
      10
    )

    let rotation = Matrix(1)
      .rotate(t * 60, Vector(0, 1, 1))
      .rotate(90, Vector(1, 0, 0))
    let translation = Matrix(1)//.translate(Vector(2, -1.3, 0))
    let scale = Matrix(1).scale(1.8)
    let model = scale * translation * rotation
    let normal_matrix = model.inverse().transpose()

    _m_matrix.set_matrix(model)
    _v_matrix.set_matrix(view)
    _p_matrix.set_matrix(projection)
    _n_matrix.set_matrix(normal_matrix)

    _shape_system.draw()
    _window.swap()
    _window.process_errors()

    if not _window.has_quit_event() then
      apply()
    else
      _window.finish()
    end

actor Main

  new create(env: Env) =>
    let game = Game.create(env)
    game()
