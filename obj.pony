
use "files"
use "itertools"
use "collections"
use "debug"

class Obj

  var _env: Env
  var _faces: Array[(U32, U32, U32)] = _faces.create()
  var _vertices: Array[F32] = _vertices.create()
  var _normals: Array[F32] = _vertices.create()

  new create(env: Env, name: String) =>
    _env = env

    try
      let file = File.create(
        FilePath(_env.root as AmbientAuth, name)?
      )

      for line in file.lines() do
        let fields: Array[String] ref = line.split()
        try
          let first_field = fields.shift()?
          match first_field
          | "f" =>
            parse_face(fields)
          | "v" =>
            _vertices.append(
              [
                fields(0)?.f32()
                fields(1)?.f32()
                fields(2)?.f32()
              ]
            )
          | "vn" => _normals.append(
              [
                fields(0)?.f32()
                fields(1)?.f32()
                fields(2)?.f32()
              ]
            )
          end
        else
          _env.err.print("invalid field: " + line)
        end
      end
    else
      _env.err.print("File may be invalid: " + name)
    end

  fun ref parse_face(fields: Array[String]) =>
    for field in fields.values() do
      let parts = field.split("/")
      try
        _faces.push((parts(0)?.u32()?, 0, parts(2)?.u32()?))
      else
        _env.err.print("unable to convert to tuple")
      end
    end

  fun ref vertices_and_normals(): Array[F32] =>
    // vertices and normals will fit in here:
    let output = Array[F32](_vertices.size() * 2)

    for face in _faces.values() do
      let vertex_index = face._1.usize()
      let normal_index = face._3.usize() - 1

      _normals.copy_to(output, normal_index * 3, (vertex_index * 3 * 2) - 3, 3)
      _vertices.copy_to(output, vertex_index * 3, vertex_index * 3 * 2, 3)
    end
    output

  fun elements(): Array[U32] =>
    let elems = Array[U32]
    for face in _faces.values() do
      // I use 0 indexing - .obj files
      // use 1 indexing. GRRRRR
      elems.push(face._1 - 1)
    end
    elems
