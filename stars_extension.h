
#include <SDL2/SDL.h>
#include <stdlib.h>

SDL_Event *make_sdl_event(void);
void free_sdl_event(SDL_Event *e);
int is_sdl_quit_event(SDL_Event *e);
void process_errors(void);
void gl_debugging(void);
