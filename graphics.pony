
// graphics.pony

use "itertools"
use "files"

class Uniform

  var _id: U32

  new create(id: U32) =>
    _id = id

  fun set_float(f: F32) =>
    @glUniform1f(_id, f)

  fun set_matrix(m: Matrix) =>
    @glUniformMatrix4fv(_id, 1, GLFalse(), m.cpointer())


class DefineAttribute

  var name: String
  var bytes: U32

  new create(name': String, bytes': U32) =>
    name = name'
    bytes = bytes'


class ShaderProgram

  var _program: U32
  var _env: Env
  var _attribute_stride: U32 = 0
  var _attribute_definitions: Array[DefineAttribute]

  new create(env: Env, vert: String, frag: String, attribute_definitions: Array[DefineAttribute]) =>
    _env = env
    _program = @glCreateProgram()
    _attribute_definitions = attribute_definitions

    // find out total stride earlier rather than later
    for def in _attribute_definitions.values() do
      _attribute_stride = _attribute_stride + def.bytes
    end

    try
      vertex_shader(vert)?
      fragment_shader(frag)?
    else
      _env.out.print("Could not compile shaders")
    end
    link()

  fun vertex_shader(name: String)? =>
    """
    TODO: make this take a file name instead.
    """
    _make_shader(name, GLVertexShader())?

  fun fragment_shader(name: String)? =>
    """
    TODO: make this take a file name instead.
    """
    _make_shader(name, GLFragmentShader())?

  fun _make_shader(name: String, kind: U32)? =>

    let source: String val = File.create(
      FilePath(_env.root as AmbientAuth, name)?
    ).read_string(4000)
    var cstring = source.cstring()

    let shader = @glCreateShader(kind)
    @glShaderSource(shader, 1, addressof cstring, NULL())
    @glCompileShader(shader)
    @glAttachShader(_program, shader)

    var status: U32 = 1
    @glGetShaderiv[None](shader, GLCompileStatus(), addressof status)
    if status != 1 then
      @error_shader_log[None](shader)
      // exit with error
    end

  fun attribute(name: String, number: U32, stride: I32, index: U32) =>
    let attr = @glGetAttribLocation(_program, name.cstring())
    if attr == -1 then _env.out.print("Attribute " + name + " not active") end
    let unit = @float_size[I32]()
    @glVertexAttribPointer(attr, number, GLFloat(), GLFalse(), stride * unit, index * unit.abs())
    @glEnableVertexAttribArray(attr)

  fun uniform(name: String): Uniform =>
    let id = @glGetUniformLocation(_program, name.cstring())
    Uniform(id)

  fun link() =>
    @glLinkProgram(_program)

    var status: U32 = 1
    @glGetProgramiv[None](_program, GLLinkStatus(), addressof status)
    if status != 1 then
      @error_program_log[None](_program)
      // exit with error
    end

  fun apply() =>
    @glUseProgram(_program)
    var index: U32 = 0
    for def in _attribute_definitions.values() do
      attribute(def.name, def.bytes, _attribute_stride.i32(), index)
      index = index + def.bytes
    end

primitive Graphics

  fun clear(r: F32, g: F32, b: F32, a: F32) =>
    @glClearColor(r, g, b, a)
    @glClear(GLColorBufferBit() or GLDepthBufferBit())

  fun time(): F32 =>
    @SDL_GetTicks().f32() / 1000.0

