#version 130

in vec3 position;
in vec3 normal;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform mat4 N;
uniform float time;

out vec3 v_normal;
out vec3 v_position;
out float Time;

void main()
{
  Time = time;
  v_normal = mat3(N) * normal;
  v_position = vec3(P * V * M * vec4(position, 1.0));
  gl_Position = P * V * M * vec4(position, 1.0);
}
