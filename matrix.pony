
use "debug"

class Vector

  var data: Pointer[Vec3]

  new create(x: F32, y: F32, z: F32) =>
    data = @glm_vec3(x, y, z)

  fun _final() =>
    @glm_free[None](data)

class Matrix

  var _data: Pointer[Mat4]

  new create(f: F32) =>
    _data = @glm_mat4(f)

  new _from_data(data: Pointer[Mat4]) =>
    _data = data

  new look_at(a: Vector, b: Vector, c: Vector) =>
    _data = @glm_look_at(a.data, b.data, c.data)

  new perspective(a: F32, b: F32, c: F32, d: F32) =>
    _data = @glm_perspective(a, b, c, d)

  fun mul(mat: Matrix): Matrix =>
    Matrix._from_data(@glm_mat4_mul(_data, mat._data))

  fun rotate(angle: F32, axis: Vector): Matrix =>
    Matrix._from_data(@glm_mat4_rotate(_data, angle, axis.data))

  fun translate(v: Vector): Matrix =>
    Matrix._from_data(@glm_mat4_translate(_data, v.data))

  fun inverse(): Matrix =>
    Matrix._from_data(@glm_mat4_inverse(_data))

  fun transpose(): Matrix =>
    Matrix._from_data(@glm_mat4_transpose(_data))

  fun scale(f: F32): Matrix =>
    Matrix._from_data(@glm_mat4_scale(_data, f))

  fun print() =>
    @glm_mat4_print(_data)

  fun cpointer(): Pointer[F32] =>
    let ptr = @glm_value_ptr(_data)
    /* Debug("---") */
    /* for x in Array[F32].from_cpointer(ptr, 16).values() do */
    /*   Debug(x) */
    /* end */
    ptr

  fun _final() =>
    @glm_free[None](_data)
